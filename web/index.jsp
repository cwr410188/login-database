<%-- 
    Document   : SelectSample2
    Created on : Sep 30, 2018, 8:46:40 PM
    Author     : lendle
--%>

<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.Connection"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <a>WELCOME TO NOTEBOOK</a><br/>
        <br/>
        <a href="AddNbook.jsp">ADD NEW EVENT</a><br/>
        <table border="1">
            <thead>
                <tr>
                    <th>EVENT</th>
                    <th>DETAIL</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <%
                    Class.forName("com.mysql.cj.jdbc.Driver");
                    Connection conn = DriverManager.getConnection("jdbc:mysql://localhost/test", "root", "");
                    Statement stmt = conn.createStatement();
                    ResultSet rs = stmt.executeQuery("SELECT * From nbook");
                    while(rs.next()){
                %>
                
                <tr>
                    <td><%=rs.getString("event")%></td>
                    <td><%=rs.getString("detail")%></td>
                    <td><a href="DeleteNbook.jsp?event=<%=rs.getString("event")%>">Delete</a></td>
                </tr>
                <%
                    }
                    %>
            </tbody>
        </table>
        <a href="UpdateNbook.jsp">UPDATE</a><br/>
        <br/>
        <a href="logout.jsp">Logout</a>
    </body>
</html>
